#!/bin/bash

apt-get update
apt-get install ros-melodic-desktop ros-melodic-fetch-description nvidia-cuda-toolkit wget curl iproute2 openssh-server -y
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh -b
source /root/miniconda3/bin/activate
conda create -n fetch python=3.8
conda install -n fetch pytorch torchvision torchaudio pytorch-cuda=12.1 -c pytorch -c nvidia -y
conda activate fetch
pip3 install opencv-python pandas zmq scikit-learn
